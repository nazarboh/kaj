
function makeHeaderStinky() {
    // Get the header
    const emptyHeader = document.getElementById("emptyHeader")
    const header = document.getElementById("appHeader")

    // Get the offset position of the navbar
    const sticky = header.offsetTop;
    emptyHeader.style.display = "none";

    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function makeSticky() {
        if (window.pageYOffset > sticky) {
            emptyHeader.style.display = "block";
            header.classList.add("sticky");
        } else {
            emptyHeader.style.display = "none";
            header.classList.remove("sticky");
        }
    }

    // When the user scrolls the page, execute function
    window.onscroll = function () {
        makeSticky()
    };
}

function makeHamburgerMenu() {
    // hamburger menu
    const hamburger = document.querySelector(".hamburger");
    const navMenu = document.querySelector(".nav-menu");

    // add active token
    hamburger.addEventListener("click", () => {
        hamburger.classList.toggle("active");
        navMenu.classList.toggle("active");
    });

    // remove active token
    document.querySelectorAll(".nav-link").forEach(n => n.addEventListener("click", () => {
        hamburger.classList.remove("active");
        navMenu.classList.remove("active");
    }));
}

// when html loaded
document.addEventListener('DOMContentLoaded', function () {
    makeHeaderStinky();
    makeHamburgerMenu();
});

class Country {
    constructor({ region, geoJson }) {
        this.region = region
        this.geoJson = geoJson
    }
}

class Continent {
    constructor({ name, countries }) {
        this.name = name
        this.countries = countries
    }
}

class World {
    constructor({ continents }) {
        this.continents = continents
    }
}

class Router {
    constructor({ pages, defaultPage }) {
        this.pages = pages;
        this.defaultPage = defaultPage;
        this.currentPage = null

        // first run
        this.route(window.location.href);

        // listen on url changes from user clicking back button
        window.addEventListener('popstate', e => {
            this.route(window.location.href);
        });

        // listen on url changes from user clicks
        window.addEventListener('click', e => {
            const element = e.target
            if (element.nodeName === 'A') {
                e.preventDefault();
                this.route(element.href);
                window.history.pushState(null, null, element.href)
            }
        });
    }

    route(urlString) {
        const url = new URL(urlString)
        const page = url.searchParams.get('page')

        if (this.currentPage) {
            this.currentPage.pageHide()
        }

        const page404 = this.pages.find(p => p.key === '404')
        const pageInstanceMatched = this.pages.find(p => p.key === (page ?? this.defaultPage))
        const currentPage = pageInstanceMatched ?? page404

        this.currentPage = currentPage
        this.currentPage.pageShow()
    }
}

class Page {
    constructor({ key, title }) {
        this.pageElement = document.querySelector(`#content`)
        this.title = title
        this.key = key
    }

    render() {
        return ``
    }

    pageShow() {
        this.pageElement.innerHTML = this.render()
        document.title = this.title
        scroll(0, 0)
    }

    pageHide() {
        this.pageElement.innerHTML = ''
    }
}

let map;

function style(feature) {
    return {
        fillColor: feature.properties.color, //getColor(feature.properties.density),
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7
    };
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: onClick
    });
}

function updateColor(elm) {
    const country = elm.childNodes[0].className;
    const region = elm.childNodes[1].className;
    const color = elm.childNodes[2].className;

    let found = false;

    // go through all geoJsons bounded to specific country and find desired country
    for (const [key, value] of Object.entries(map.geoJsonLayerDict)) {

        if (key != country) {
            continue;
        }

        // find derired region in geoJson
        for (const [i, layer] of Object.entries(value._layers)) {
            
            // change color, save the change into localStorage
            if (layer.feature.properties.region === region) {
                layer.setStyle({ fillColor: color });

                localStorage.removeItem(country + ':' + region + ':' + 'color=' + layer.feature.properties.color)
                layer.feature.properties.color = color
                localStorage.setItem(country + ':' + region + ':' + 'color=' + color, "");

                found = true;
                break;
            }
        }

        if (found) {
            break;
        }
    }
}

function updateText(elm) {
    const country = elm.childNodes[1].className;
    const region = elm.childNodes[2].className;

    const form = document.getElementById('save');

    let found = false;

    const input = form.elements['text'];

    // go through all geoJsons bounded to specific country and find desired country
    for (const [key, value] of Object.entries(map.geoJsonLayerDict)) {

        if (key != country) {
            continue;
        }

        // find derired region in geoJson
        for (const [i, layer] of Object.entries(value._layers)) {
            
            // change text, save the change into localStorage 
            if (layer.feature.properties.region === region) {
                const div = document.getElementById('note');

                let text = input.value;

                if (text !== "") {
                    text = text + '<br/>' + '<br/>'
                }

                div.innerHTML = text

                localStorage.removeItem(country + ':' + region + ':' + 'text=' + layer.feature.properties.text)
                layer.feature.properties.text = input.value
                localStorage.setItem(country + ':' + region + ':' + 'text=' + input.value, "");

                console.log(input.value.toString())

                found = true;
                break;
            }
        }

        if (found) {
            break;
        }
    }

}

function deleteMarker(elm) {
    const lat = elm.childNodes[1].className;
    const lng = elm.childNodes[2].className;

    // find and delete a marker using position on the map
    for (const marker of map.markersDict) {
        if (lat == marker.lat && lng == marker.lng) {
            map.removeLayer(marker.marker)
            localStorage.removeItem('marker:' + lat + ',' + lng)
            break
        }
    }
}

function makeMarker(elm) {
    const form = document.getElementById('mark');
    const lat = form.childNodes[2].className;
    const lng = form.childNodes[3].className;


    marker = L.marker([lat, lng])
        .bindPopup(
            '<button type="button" onclick="deleteMarker(this)">' +
            'DELETE' +
            '<div class="' + lat + '"></div>' +
            '<div class="' + lng + '"></div>' +
            '</button>'
        ).addTo(map);
    
    map.markersDict.push({ lat: lat, lng: lng, marker: marker })
    localStorage.setItem('marker:' + lat + ',' + lng, "");
}

function onClick(e) {

    text = e.target.feature.properties.text === "" ? "" : e.target.feature.properties.text + '<br/>' + '<br/>'

    // popup bounded to specific region on click
    var popup = L.popup()
        .setLatLng(e.latlng)
        .setContent(
            '<h2>' + e.target.feature.properties.region + '</h2>' +
            '<div class="palette">' +
            '<button type="button" class="change" onclick="updateColor(this)" style="background-color:red;">' +
            //'RED' + 
            '<div class="' + e.target.feature.properties.country + '"></div>' +
            '<div class="' + e.target.feature.properties.region + '"></div>' +
            '<div class="red"></div>' +
            '</button>' +
            '<button type="button" class="change" onclick="updateColor(this)" style="background-color:orange;">' +
            //'ORANGE' + 
            '<div class="' + e.target.feature.properties.country + '"></div>' +
            '<div class="' + e.target.feature.properties.region + '"></div>' +
            '<div class="orange"></div>' +
            '</button>' +
            '<button type="button" class="change" onclick="updateColor(this)" style="background-color:yellow;">' +
            //'YELLOW' + 
            '<div class="' + e.target.feature.properties.country + '"></div>' +
            '<div class="' + e.target.feature.properties.region + '"></div>' +
            '<div class="yellow"></div>' +
            '</button>' +
            '<button type="button" class="change" onclick="updateColor(this)" style="background-color:blue;">' +
            //'BLUE' + 
            '<div class="' + e.target.feature.properties.country + '"></div>' +
            '<div class="' + e.target.feature.properties.region + '"></div>' +
            '<div class="blue"></div>' +
            '</button>' +
            '<button type="button" class="change" onclick="updateColor(this)" style="background-color:grey;">' +
            //'GREY' + 
            '<div class="' + e.target.feature.properties.country + '"></div>' +
            '<div class="' + e.target.feature.properties.region + '"></div>' +
            '<div class="grey"></div>' +
            '</button>' +
            //'WHITE' +
            '<button type="button" class="change" onclick="updateColor(this)" style="background-color:white;">' +
            '<div class="' + e.target.feature.properties.country + '"></div>' +
            '<div class="' + e.target.feature.properties.region + '"></div>' +
            '<div class="white"></div>' +
            '</button>' +
            // 'PURPLE' + 
            '<button type="button" class="change" onclick="updateColor(this)" style="background-color:purple;">' +
            '<div class="' + e.target.feature.properties.country + '"></div>' +
            '<div class="' + e.target.feature.properties.region + '"></div>' +
            '<div class="purple"></div>' +
            '</button>' +
            // 'GREEN' + 
            '<button type="button" class="change" onclick="updateColor(this)" style="background-color:green;">' +
            '<div class="' + e.target.feature.properties.country + '"></div>' +
            '<div class="' + e.target.feature.properties.region + '"></div>' +
            '<div class="green"></div>' +
            '</button>' +
            // 'PINK' + 
            '<button type="button" class="change" onclick="updateColor(this)" style="background-color:pink;">' +
            '<div class="' + e.target.feature.properties.country + '"></div>' +
            '<div class="' + e.target.feature.properties.region + '"></div>' +
            '<div class="pink"></div>' +
            '</button>' +
            // 'BLACK' + 
            '<button type="button" class="change" onclick="updateColor(this)" style="background-color:black;">' +
            '<div class="' + e.target.feature.properties.country + '"></div>' +
            '<div class="' + e.target.feature.properties.region + '"></div>' +
            '<div class="black"></div>' +
            '</button>' +
            '</div>' +
            '<h2>Your note</h2>' +
            '<div id="note">' +
            text +
            '</div>' +
            '<form id="save">' +
            '<input id="text" type="text" value="' + e.target.feature.properties.text + '">' +
            '<button type="button" onclick="updateText(this)">' +
            'SAVE' +
            '<div class="' + e.target.feature.properties.country + '"></div>' +
            '<div class="' + e.target.feature.properties.region + '"></div>' +
            '</button>' +
            '</form>' +
            // '<br></br>' +
            '<form id="mark">' +
            '<h2>Marker</h2>' +
            '<input type="checkbox" onchange="makeMarker(this)">' +
            '<div class="' + e.latlng.lat + '"></div>' +
            '<div class="' + e.latlng.lng + '"></div>' +
            '</form>'
        )
        .openOn(map);
}

function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    layer.bringToFront();
}

function resetHighlight(e) {
    map.geoJsonLayerDict[e.target.feature.properties.country].resetStyle(e.target);
}

class PageHome extends Page {
    constructor({ key, title }) {
        super({ key, title })

        this.initWorld()
    }

    createLeafletMap() {
        map = L.map('map').setView([40, -100], 4);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoiYm9oaW11cyIsImEiOiJjbDNoa3p6YTkxZDZjM2twOGluZng1NTNwIn0.na6MtLB5_4Eb5zHNQD1XQA'
        }).addTo(map);
    }

    initWorld() {
        this.world = new World({
            continents: [
                new Continent({
                    name: 'NA',
                    countries: [
                        new Country({
                            region: 'USA',
                            geoJson: geoJsonUSA
                        })
                    ]
                })
            ]
        });
    }

    loadDefaultWorld() {
        map.geoJsonLayerDict = {}
        map.markersDict = []

        this.world.continents.forEach(continent => {
            continent.countries.forEach(country => {
                map.geoJsonLayerDict[country.region] = L.geoJson(country.geoJson, {
                    style: style,
                    onEachFeature: onEachFeature
                }).addTo(map);
            })
        })
    }

    loadMarkers() {

        // go through all items in localStorage, find markers using parsing of key
        for (const [key, value] of Object.entries(localStorage)) {
            const splitted = key.split(':')

            if (splitted.length != 2) {
                continue
            }

            if (splitted[0] === 'marker') {
                const coordinates = splitted[1].split(',')

                const lat = parseFloat(coordinates[0])
                const lng = parseFloat(coordinates[1])


                let marker = L.marker([lat, lng])
                    .bindPopup(
                        '<button type="button" onclick="deleteMarker(this)">' +
                        'DELETE' +
                        '<div class="' + lat + '"></div>' +
                        '<div class="' + lng + '"></div>' +
                        '</button>'
                    ).addTo(map);

                map.markersDict.push({ lat: lat, lng: lng, marker: marker })
            }
        }
    }


    // USA:Alabama:color=#808080
    // USA:Alabama:tetx=someText
    loadUserWorld() {
        // go through all items in localStorage, find all regoins specifics using parsing of key
        for (const [key, value] of Object.entries(localStorage)) {
            let splitted = key.split(':')


            if (splitted.length != 3) {
                continue
            }

            const country = splitted[0]
            const region = splitted[1]
            const attribut = splitted[2]

            let found = false;
            
            // go through all geoJsons bounded to specific country and find desired country
            for (const [key, value] of Object.entries(map.geoJsonLayerDict)) {
                if (key != country) {
                    continue;
                }

                // find derired region in geoJson
                for (const [i, layer] of Object.entries(value._layers)) {

                    if (layer.feature.properties.region === region) {

                        const splittedAtribut = attribut.split('=')

                        const attributName = splittedAtribut[0]
                        const attributValue = splittedAtribut[1]
                        
                        // change default attributs in geoJson
                        if (attributName === 'color') {
                            layer.setStyle({ fillColor: attributValue });
                            layer.feature.properties.color = attributValue
                        }
                        else if (attributName === 'text') {
                            layer.feature.properties.text = attributValue
                        }


                        found = true;
                        break;
                    }
                }

                if (found) {
                    break;
                }
            }
        }
    }

    pageShow() {
        this.pageElement.innerHTML = this.render()
        document.title = this.title

        this.createLeafletMap()
        this.loadDefaultWorld()

        this.loadMarkers()
        this.loadUserWorld()

        scroll(0, 0)
    }

    render() {
        return `
            <home>
                <div id="map"></div>
            </home>
        `
    }
}

class PageHelp extends Page {
    render() {
        return `
            <help> 
                <h2>HELP</h2>
                <p>C'mon, man! You're grown man, help yourself on your own!</p>

                <h2>DRAG N DROP INSTED!</h2>
                <div id="dnd"></div>
				<ul id="imgInfo"></ul>
				<div id="imgCnt"></div>
            </help>
        `
    }

    loadImage() {
        for (const [key, value] of Object.entries(localStorage)) {
            console.log([key, value])
        }

        const image = localStorage.getItem('image');
        const info = localStorage.getItem('info');

        const imgContent = document.querySelector("#imgCnt");
        const imgInfo = document.querySelector("#imgInfo");

        if (image != null) {
            const i = new Image();
            i.src = image
            imgContent.appendChild(i);
            imgInfo.innerHTML = info
        }
    }

    pageShow() {
        super.pageShow()

        const dnd = document.querySelector("#dnd");
        dnd.addEventListener("drop", this.onDrop);
        dnd.addEventListener("dragover", this.onDragOver);

        this.loadImage()
    }

    pageHide() {
        dnd.removeEventListener("drop", this.onDrop);
        dnd.removeEventListener("dragover", this.onDragOver);

        super.pageHide()
    }

    onDragOver(e) {
        e.preventDefault();
    }

    onDrop(e) {
        e.preventDefault();
        const files = e.dataTransfer.files;

        const imgContent = document.querySelector("#imgCnt");
        const imgInfo = document.querySelector("#imgInfo");
        imgInfo.innerHTML = ''

        for (const f of files) {
            if (!f.type.includes("image")) {
                continue
            }

            const fr = new FileReader();
            fr.addEventListener("load", e => {
                const i = new Image();
                i.addEventListener("load", e => {
                    i.width = Math.min(i.naturalWidth, window.innerWidth - 20);
                })
                i.src = fr.result;
                imgContent.removeChild(imgContent.lastChild)
                imgContent.appendChild(i);

                const li = document.createElement('li');
                li.innerText = `type: ${f.type} | size: ${f.size} | name: ${f.name}`
                imgInfo.appendChild(li);

                //save new
                localStorage.setItem("image", fr.result);
                localStorage.setItem("info", li.innerText);
            })
            fr.readAsDataURL(f);
        }
    }
}

function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    var dataURL = canvas.toDataURL("image/png");

    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

class PageAbout extends Page {
    render() {
        return `
            <about> 
                <h2>ABOUT</h2>
                <p>
                    At Lumierium, we believe there is a better way to do traveling. A more
                        valuable, less invasive way where travelers are save rather than
                        spend their moneiz. We're obsessively passionate about it, and our mission is to

                        help people achieve it. We focus on interactive TOGOs. 
                        It's one of the least understood and least transparent aspects
                        of great traveling, and we see that as an opportunity: We're excited
                        to upgrade traveling experince for everyone through our software and
                        community.
                </p>

                
                <h2>OUR FOUNDING</h2>
                <p>
                    Lumierium was founded by Bohdan Nazarenko and his imagination friends in 2020. It was called
                    Lumiere back then, and started as a semestral work at the unitersity. We launched and hosted the first 
                    version of website on cousin's server, the first version sucked though, but we did't give up
                    and after 5 years of struggle we managed to build really good product with online community
                    and big future. Great thanks to all who've been supporting us all these years!
                </p>
            </about>
        `
    }
}

class PageNotFound extends Page {
    render() {
        return `
            <undetected>
                <h2>404 Page Not Found</h2>
            </undetected>
        `
    }
}

class PageRick extends Page {
    render() {
        return `
            <rick>
                <div>
                    <h2>YOU'VE BEEN RICK ROLLED, SUCKER!</h2>

                    <video autoplay="autoplay" controls="controls">
                        <source src="data/rick.mp4" type="video/mp4"  />
                        Your browser does not support the video tag.
                    </video>
                </div>
            </rick>
        `
    }
}

new Router({
    pages: [
        new PageHome({ key: 'home', title: 'Home' }),
        new PageHelp({ key: 'help', title: 'Help' }),
        new PageAbout({ key: 'about', title: 'About' }),
        new PageRick({ key: 'rick', title: 'YOU\'VE BEEN RICK ROLLED' }),
        new PageNotFound({ key: '404', title: 'Page not found' })
    ],
    defaultPage: 'home'
});
